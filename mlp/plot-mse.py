#! /bin/env python3

from mlp import MLP

try:
    import matplotlib
    import matplotlib.pyplot as plt
    import matplotlib.lines as mlines
    with MLP() as rna:
        rna.load("mlp.model")

        print("score train:", rna.score_train)
        print("score test:", rna.score_test)

        fig, ax = plt.subplots()
        ax.plot(rna.mse_train, label="train")
        ax.plot(rna.mse_test, label="test")
        ax.set_title("mse")
        leg = plt.legend(loc='best', ncol=1)
        plt.show()
except Exception as e:
    print("Not possible to plot the graphic:", e)