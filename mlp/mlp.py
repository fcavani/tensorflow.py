# /bin/env python3

import os
import numpy as np
import tensorflow as tf
from sklearn.preprocessing import Imputer, MinMaxScaler

class MLP():
    def __init__(
        self,
        mse_target=1e-4,
        max_epochs=1000,
        hidden_layer_sizes=None,
        output_fn=tf.identity,
        verbose=False,
        learning_rate=0.001,
        random_seed=None,
    ):
        self.net = None
        self._initialized = False

        self.mse_target = mse_target
        self.max_epochs = max_epochs
        self._verbose = verbose
        if hidden_layer_sizes is not None:
            if type(hidden_layer_sizes) is not tuple:
                raise Exception("invalid hidden layer architecure")
            if len(hidden_layer_sizes) <= 0:
                raise Exception("invalid hidden layer architecure, length is zero")
            self.hidden_layer_sizes = hidden_layer_sizes
        self.output_fn = output_fn
        self.learning_rate = learning_rate
        if random_seed is not None:
            tf.set_random_seed(random_seed)

    def _initializers(self, x_shape, y_shape):
        if hasattr(self, "_initialized") and self._initialized:
            return

        self._x_shape = x_shape
        self._y_shape = y_shape

        # Initializers
        sigma = 1
        weight_initializer = tf.variance_scaling_initializer(
            mode="fan_avg",
            distribution="uniform",
            scale=sigma
        )
        bias_initializer = tf.zeros_initializer()

        # Data placeholders
        self._X = tf.placeholder(dtype=tf.float32, shape=[None, x_shape[1]])
        self._Y = tf.placeholder(dtype=tf.float32, shape=[None, y_shape[1]])

        n_target = y_shape[1]

        neurons = [x_shape[1]]
        hiddens = [self._X]

        for layer in self.hidden_layer_sizes:
            n_neurons = 0
            activation = None
            if type(layer) is int:
                n_neurons = layer
                activation = tf.nn.relu
            elif type(layer) is tuple:
                if len(layer) != 2:
                    raise Exception("invalid layer tuple size")
                n_neurons = layer[0]
                activation = layer[1]
            else:
                raise Exception("invalid type of layer")
            # Layer: Variables for hidden weights and biases
            W_hidden = tf.Variable(
                weight_initializer([neurons[-1], n_neurons])
            )
            neurons.append(n_neurons)
            bias_hidden = tf.Variable(bias_initializer([n_neurons]))
            # Hidden layer
            hiddens.append(
                activation(
                    tf.add(tf.matmul(hiddens[-1], W_hidden), bias_hidden)
                )
            )

        # Output layer: Variables for output weights and biases
        W_out = tf.Variable(weight_initializer([neurons[-1], n_target]))
        bias_out = tf.Variable(bias_initializer([n_target]))

        # Output layer
        self._out = self.output_fn(
            tf.add(tf.matmul(hiddens[-1], W_out), bias_out))

        # Cost function
        self._mse = tf.reduce_mean(tf.squared_difference(self._out, self._Y))

        # Optimizer
        self._opt = tf.train.AdamOptimizer(
            learning_rate=self.learning_rate,
        ).minimize(self._mse)

        # Saver
        self._saver = tf.train.Saver()

        # Session
        self._start_tf_session()

        # Init
        self.net.run(tf.global_variables_initializer())

        self._initialized = True

    def fit(self, X, y):
        if type(X) is not np.ndarray:
            raise TypeError()
        if type(y) is not np.ndarray:
            raise TypeError()
        if len(X.shape) < 2:
            raise Exception("invalid matrix shape")
        if len(y.shape) < 2:
            raise Exception("invalid matrix shape")
        self._initializers(X.shape, y.shape)

        # Build train and test data set
        train_start = 0
        train_end = int(np.floor(0.8 * X.shape[0]))
        test_start = train_end + 1
        test_end = X.shape[0]
        X_data_train = X[np.arange(train_start, train_end), :]
        X_test = X[np.arange(test_start, test_end), :]
        y_data_train = y[np.arange(train_start, train_end), :]
        y_test = y[np.arange(test_start, test_end), :]

        # Fit neural net
        batch_size = 256
        mse_train = []
        mse_test = []

        mse_actual = np.finfo(np.float32).max
        epochs = 0

        while mse_actual > self.mse_target and epochs < self.max_epochs:
            # Shuffle training data
            shuffle_indices = np.random.permutation(
                np.arange(X_data_train.shape[0]))
            X_train = X_data_train[shuffle_indices]
            y_train = y_data_train[shuffle_indices]

            if X.shape[0] <= batch_size:
                self.net.run(
                    self._opt,
                    feed_dict={self._X: X_train, self._Y: y_train})
                mse_train.append(
                    self.net.run(
                        self._mse,
                        feed_dict={self._X: X_train, self._Y: y_train}))
                mse_test.append(
                    self.net.run(
                        self._mse,
                        feed_dict={self._X: X_test, self._Y: y_test}))
                mse_actual = mse_train[-1]

                if np.mod(epochs, 50) == 0 and self._verbose:
                    print("Epoch:", epochs)
                    print('MSE Train:', mse_train[-1])
                    print('MSE Test:', mse_test[-1])
            else:
                # Minibatch training
                for i in range(0, X.shape[0] // batch_size):
                    start = i * batch_size
                    batch_x = X_train[start:start + batch_size]
                    batch_y = y_train[start:start + batch_size]
                    # Run optimizer with batch
                    self.net.run(
                        self._opt,
                        feed_dict={self._X: batch_x, self._Y: batch_y})
                    mse_train.append(
                        self.net.run(
                            self._mse,
                            feed_dict={self._X: X_train, self._Y: y_train}))
                    mse_test.append(
                        self.net.run(
                            self._mse,
                            eed_dict={self._X: X_test, self._Y: y_test}))
                    mse_actual = mse_train[-1]

                    if np.mod(epochs, 50) == 0 and self._verbose:
                        print("Epoch:", epochs)
                        print('MSE Train:', mse_train[-1])
                        print('MSE Test:', mse_test[-1])

            epochs += 1
        self.mse_train = mse_train
        self.mse_test = mse_test
        self.score_train = self.score(X_train, y_train)
        self.score_test = self.score(X_test, y_test)
        return self

    def score(self, X, y_true):
        """ R^2 coefficient
            The best possible score is 1.0 and it can be negative
            (because the model can be arbitrarily worse). A constant
            model that always predicts the expected value of y,
            disregarding the input features, would get a R^2 score of 0.0.
            (from sklearn documentation)
        """
        if not self._initialized:
            raise Exception("Not initialized. Run fit first.")
        if type(X) is not np.ndarray:
            raise TypeError()
        if type(y_true) is not np.ndarray:
            raise TypeError()
        if len(X.shape) < 2:
            raise Exception("invalid matrix shape")
        if len(y_true.shape) < 2:
            raise Exception("invalid matrix shape")
        y_pred = self.net.run(self._out, feed_dict={self._X: X})
        u = ((y_true - y_pred) ** 2).sum()
        v = ((y_true - y_true.mean()) ** 2).sum()
        return (1 - u/v)

    def predict(self, X):
        if not self._initialized:
            raise Exception("Not initialized. Run fit first.")
        if type(X) is not np.ndarray:
            raise TypeError()
        if len(X.shape) < 2:
            raise Exception("invalid matrix shape")
        # Prediction
        pred = self.net.run(self._out, feed_dict={self._X: X})
        return pred

    def save(self, filename):
        fname = os.path.basename(filename)
        directory = os.path.dirname(filename)
        # _, file_extension = os.path.splitext(filename)
        if directory != "":
            directory += "/"
        model_path = directory + fname + ".tfmodel" 
        self._model_path = model_path
        import pickle
        with open(filename, mode='wb') as outfile:
            pickle.dump(self, outfile, protocol=4)

    def load(self, filename):
        import pickle
        with open(filename, mode='rb') as f:
            rna = pickle.load(f)
            self.__dict__.update(rna.__dict__)
            self._initializers(self._x_shape, self._y_shape)
    
    def _save_tf(self, model_file=""):
        if model_file == "":
            model_file = self._model_path
        #path = os.path.abspath(model_file)
        if not os.path.isdir(model_file):
            os.mkdir(model_file)
        model_file = model_file + "/"
        self._save_path = self._saver.save(self.net, model_file)
        
    def _load_tf(self, model_file=""):
        if model_file == "":
            model_file = self._save_path
        self._saver.restore(self.net, model_file)

    def _serialize_hidden_layers(self, hidden):
        out = []
        for h in hidden:
            if type(h) is tuple:
                if len(h) != 2:
                    raise Exception("invalid hidden layer tuple")
                if type(h[0]) is not int:
                    raise Exception("invalid hidden layer neurons number")
                if not hasattr(h[1], '__call__'):
                    raise Exception("invalid hidden layer activation function")
                out.append([h[0], _ser_fn_activation(h[1])])
            elif type(h) is int:
                out.append(h)
            else:
                raise Exception("invalid type")
        return tuple(out)

    def _unserialize_hidden_layers(self, hidden):
        out = []
        for h in hidden:
            if type(h) is list:
                if len(h) != 2:
                    raise Exception("invalid hidden layer tuple")
                if type(h[0]) is not int:
                    raise Exception("invalid hidden layer neurons number")
                if type(h[1]) is not str:
                    raise Exception("invalid hidden layer activation function")
                out.append(tuple([h[0], _unser_fn_activation(h[1])]))
            elif type(h) is int:
                out.append(h)
            else:
                raise Exception("invalid type:", type(h))
        return tuple(out)

    def __getstate__(self):
        if not self._initialized:
            raise Exception("Not initialized. Run fit first.")
        state = {
            "mse_target": self.mse_target,
            "max_epochs": self.max_epochs,
            "_verbose": self._verbose,
            "_x_shape": self._x_shape,
            "_y_shape": self._y_shape,
            "mse_train": self.mse_train,
            "mse_test": self.mse_test,
            "score_train": self.score_train,
            "score_test": self.score_test,
            "hidden_layer_sizes":
                self._serialize_hidden_layers(self.hidden_layer_sizes),
            "output_fn": _ser_fn_activation(self.output_fn),
            "learning_rate": self.learning_rate,
        }
        if hasattr(self, "_model_path"):
            self._save_tf()
            state["_model_path"] = self._model_path
            state["_save_path"] = self._save_path
        else:
            print("### No path to save the model")
        return state

    def __setstate__(self, state):
        # Restore instance attributes (i.e., filename and lineno).
        hl = self._unserialize_hidden_layers(state["hidden_layer_sizes"])
        state["hidden_layer_sizes"] = hl
        state["output_fn"] = _unser_fn_activation(state["output_fn"])
        self.__dict__.update(state)
        # Restore the previously opened file's state. To do so, we need to
        # reopen it and read from it until the line count is restored.
        self._initializers(self._x_shape, self._y_shape)
        if hasattr(self, "_save_path"):
            # Load model
            self._load_tf()
        else:
            print("### no model to load")

    def __enter__(self):
        self._initialized = False
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if hasattr(self, "net") and self.net is not None:
            self.net.close()

    def close(self):
        """" 
            Closes the RNA.
            Closes the underlay session with TensorFlow.
        """
        if hasattr(self, "net") and self.net is not None:
            self.net.close()

    def _start_tf_session(self):
        if not hasattr(self, "net"):
            self.net = tf.InteractiveSession()
        if self.net is None:
            self.net = tf.InteractiveSession()



def _ser_fn_activation(fn):
    """
        Return the name of one function.
    """
    return fn.__name__


def _unser_fn_activation(name):
    """
        Return the function with name.
    """
    if name == "relu":
        return tf.nn.relu
    elif name == "sigmoid":
        return tf.nn.sigmoid
    elif name == "identity":
        return tf.identity
    elif name == "round":
        return tf.round
    raise Exception("unknown function")

def _make_matrix_classes(y):
    y_max = max(y)[0]
    out = np.zeros((y.shape[0], y_max + 1), dtype=int)
    for i in range(out.shape[0]):
        out[i, y[i]] = 1
    return out

if __name__ == '__main__':
    from sklearn.datasets import load_iris
    from sklearn.datasets import load_linnerud
    from sklearn.model_selection import StratifiedKFold
    from sklearn.model_selection import ShuffleSplit

    np.random.seed(0)

    with MLP(
        mse_target=1e-4,
        max_epochs=100000,
        hidden_layer_sizes=(16, 100, (12, tf.nn.sigmoid)),
        output_fn=tf.nn.sigmoid,
        verbose=False,
    ) as rna:
        iris = load_iris()
        data = iris["data"]
        target = iris["target"]

        skf = StratifiedKFold(n_splits=4)
        train_index, test_index = next(iter(skf.split(data, target)))

        X_train = data[train_index]
        y_train = target[train_index]
        X_test = data[test_index]
        y_test = target[test_index]

        X_train = np.array(X_train)
        y_train = np.array(y_train)
        X_test = np.array(X_test)
        y_test = np.array(y_test)

        y_train = np.reshape(y_train, (y_train.shape[0], 1))
        y_test = np.reshape(y_test, (y_test.shape[0], 1))

        y_train = _make_matrix_classes(y_train)
        y_test = _make_matrix_classes(y_test)

        rna.fit(X_train, y_train)
        y_pred = rna.predict(X_test)
        s = rna.score(X_test, y_test)
        print("score: {}".format(s))
    
    with MLP(
        mse_target=0.05,
        max_epochs=500000,
        hidden_layer_sizes=(12, (6, tf.nn.sigmoid)),
        output_fn=tf.identity,
        verbose=True,
        learning_rate=1e-6,
        random_seed=123,
    ) as rna:
        linenerud = load_linnerud()
        data = linenerud["data"]
        target = linenerud["target"]

        ss = ShuffleSplit()
        train_index, test_index = next(iter(ss.split(data)))

        X = np.array(data)
        y = np.array(target)

        norm_x = MinMaxScaler(feature_range=(0.0, 1.0), copy=True)
        norm_y = MinMaxScaler(feature_range=(0.0, 1.0), copy=True)

        X = norm_x.fit_transform(X)
        y = norm_y.fit_transform(y)

        X_train = X[train_index, :]
        y_train = y[train_index, :]
        X_test = X[test_index, :]
        y_test = y[test_index, :]

        rna.fit(X_train, y_train)
        s = rna.score(X_test, y_test)
        print("score: {}".format(s))
        
        y_pred = rna.predict(X_test)
        print(norm_y.inverse_transform(y_test))
        print(norm_y.inverse_transform(y_pred))
        rna.save("mlp.model")
